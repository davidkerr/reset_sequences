#!/usr/bin/perl

use DBI;
use DBD::Pg;
use Getopt::Long;
use strict;


my ( $db, $dbname, $schema, $username, $password, $sequence_sql, $sequence_cur, $sequence_rec, $reset_cur, $reset_sql, $username_param, $schema_param);

GetOptions ('database=s' => \$dbname, 
            'schema=s' => \$schema,
	    'password=s' => \$password,
	    'username=s' => \$username );

if ( ! $dbname  ){
	print "Usage: reset_requences.pl -d <database> [-s <schema>] [-u <user>] [-p <password>]\n";
	exit 0;
}


$db = DBI->connect("DBI:Pg:dbname=$dbname","$username","$password", {'RaiseError' => 1});
if ( !defined($db) ){
	print STDERR "Connection to database failed\n";
	print STDERR $db->errstr, "\n";
	exit 1;
}


$sequence_sql = << "EOH";
select a.table_name as tabname, a.column_name as colname, a.column_default as coldef , a.table_schema as tabsch
from information_schema.columns a, pg_tables b 
where a.table_name = b.tablename and 
a.table_schema like ? and 
b.tableowner like ? and 
a.column_default like '%nextval%'
EOH

$sequence_cur = $db->prepare($sequence_sql);

$schema_param   = $schema ? $schema : '%';
$username_param = $username ? $username : '%';

$sequence_cur->execute($schema_param, $username_param);

while ( $sequence_rec = $sequence_cur->fetchrow_hashref() ){
	my $seq = $$sequence_rec{"coldef"};
	$seq =~ s/nextval\(\'//;
	$seq =~ s/\'\:\:regclass\)//;
	print "select setval('" . $seq . "', max(" . $$sequence_rec{"colname"} . ")) from " . $$sequence_rec{"tabsch"} . "." . $$sequence_rec{"tabname"} . ";\n";
	my $col_id = $$sequence_rec{"colname"};
	my $tab_name = $$sequence_rec{"tabname"};
	my $tab_sch = $$sequence_rec{"tabsch"};
	$db->do("SELECT setval('$seq', max($col_id)) from $tab_sch.$tab_name");
}
